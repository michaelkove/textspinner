<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class SpinGroup extends Model {

	use CrudTrait;

    /*
	|--------------------------------------------------------------------------
	| GLOBAL VARIABLES
	|--------------------------------------------------------------------------
	*/
	protected $table = 'spin_groups';
	protected $primaryKey = 'id';
	protected $fillable = [
		'order',
		'wrap_tags_json',
		'spin_id',
		'label',
		'type',
	];
	public $timestamps = true;

	/*
	|--------------------------------------------------------------------------
	| FUNCTIONS
	|--------------------------------------------------------------------------
	*/

	/*
	|--------------------------------------------------------------------------
	| RELATIONS
	|--------------------------------------------------------------------------
	*/
	public function spin()
	{
		return $this->belongsTo('\App\Models\Spin');
	}

	public function sentences()
	{
		return $this->belongsToMany('\App\Models\Sentence', 'spin_groups_sentences')->withPivot('order');
	}
	/*
	|--------------------------------------------------------------------------
	| SCOPES
	|--------------------------------------------------------------------------
	*/

	/*
	|--------------------------------------------------------------------------
	| ACCESORS
	|--------------------------------------------------------------------------
	// */
	// public function add_sentences()
	// {
 //        return '<a href="/admin/spin/'.$this->id.'/add-sentences" data-toggle="tooltip" title="Add Sentences to this spin" class="btn btn-xs btn-info"><i class="fa fa-refresh" ></i> Add Sentences</a>';
	// }
	/*
	|--------------------------------------------------------------------------
	| MUTATORS
	|--------------------------------------------------------------------------
	*/
}