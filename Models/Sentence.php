<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class Sentence extends Model {

	use CrudTrait;

    /*
	|--------------------------------------------------------------------------
	| GLOBAL VARIABLES
	|--------------------------------------------------------------------------
	*/
	protected $table = 'sentences';
	protected $primaryKey = 'id';
	protected $fillable = [
		'spincode'
	];
	public $timestamps = true;

	/*
	|--------------------------------------------------------------------------
	| FUNCTIONS
	|--------------------------------------------------------------------------
	*/

	/*
	|--------------------------------------------------------------------------
	| RELATIONS
	|--------------------------------------------------------------------------
	*/
	public function spin_groups()
	{
		return $this->belongsToMany('\App\Models\SpinGroup', 'spin_groups_sentences')->withPivot('order');
	}

	public function rules()
	{
		return $this->hasMany('\App\Models\SpinRule');
	}
	/*
	|--------------------------------------------------------------------------
	| SCOPES
	|--------------------------------------------------------------------------
	*/

	/*
	|--------------------------------------------------------------------------
	| ACCESORS
	|--------------------------------------------------------------------------
	// */
	// public function add_sentences()
	// {
 //        return '<a href="/admin/spin/'.$this->id.'/add-sentences" data-toggle="tooltip" title="Add Sentences to this spin" class="btn btn-xs btn-info"><i class="fa fa-refresh" ></i> Add Sentences</a>';
	// }
	/*
	|--------------------------------------------------------------------------
	| MUTATORS
	|--------------------------------------------------------------------------
	*/
}