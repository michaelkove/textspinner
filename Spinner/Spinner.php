<?php
namespace App\Spinner;

use App\Models\Product;
use App\Models\ProductType;
use App\Models\SyncQueue;
use App\Models\ProductMagentoDetail;

use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Writer\WriterFactory;

use Box\Spout\Common\Type;


use Maatwebsite\Excel\Facades\Excel;

use App\Helper\Unify;

class Spinner
{

    public $product;
    public $spin;
    public $variables;
    private $_attributes;



    public function __construct($data)
    {
        // $this->userId = \Auth::id();
        $this->product = $data['product'];
        $this->spin = $data['spin'];
        $this->variables = $data['variables'];
        $allAtributes = \App\Models\Attribute::get();
        $this->_attributes = [];
        foreach ($allAtributes as $attr) {
            $this->_attributes[$attr->column_header] = $attr;
        }
    }

    public function process()
    {
        $paragraph = "";
        $sentenceText = "";
        foreach ($this->spin->spin_groups()->orderBy('order','asc')->get() as $spinGroup) {
            if($spinGroup->type !== 'SENTENCE') {
                $paragraph .= str_replace(' >', '>', $spinGroup->label);
            } else {

                $sentence = $this->_get_random($spinGroup->sentences->toArray());

                if($sentence && isset($sentence['spincode'])) {
                    $sentenceText = $sentence['spincode'];
                    if(preg_match_all('/\[\!(.*?)\!\]/i', $sentenceText, $matched, PREG_SET_ORDER)) {
                        foreach ($matched as $match) {
                            $find = $match[0];
                            $replace = $this->_apply_variable_rules($match[1]);
                            $sentenceText = str_replace($find, $replace, $sentenceText);
                        }
                        // dd($sentenceText);
                        // dd($matched);
                    }
                    if(preg_match_all('/\{\{(.*?)\}\}/i', $sentenceText, $wordSpins, PREG_SET_ORDER)) {
                        foreach ($wordSpins as $match) {
                            $find = $match[0];
                            $wordArray = explode('|', $match[1]);
                            $replace = $this->_get_random($wordArray);
                            $sentenceText = str_replace($find, $replace, $sentenceText);
                        }
                        // dd($sentenceText);
                        // dd($matched);
                    }
                    $sentenceText = str_replace('  ', ' ', $sentenceText);
                    $attributes = json_decode($this->product->attributes, true);
                    if($attributes && count($attributes)){
                        foreach ($attributes as $header => $attribute) {
                            $sentenceText = $this->_replace_attribute($sentenceText, $header, $attribute);
                        }
                    }

                    $tags = json_decode($spinGroup->wrap_tags_json, true);
                    if(count($tags)) {
                        $endTags = array_reverse($tags);
                        $openningTags = "";
                        $closingTags = "";
                        foreach ($tags as $tag) {
                            $openningTags .= "<{$tag['tag']} class='{$tag['classes']}'>";
                        }
                        foreach ($endTags as $tag) {
                            $closingTags .= "</{$tag['tag']}>";
                        }
                        $sentenceText = $openningTags.$sentenceText.$closingTags;
                    }
                }
                $paragraph .= $sentenceText." ";
            }
        }
        return htmlspecialchars_decode($paragraph);

    }

    private function _replace_attribute($text, $header, $attribute)
    {
        if(!is_array($attribute)) {
            $case = (isset($this->_attributes[$header])) ? $this->_attributes[$header] : null;
            if($case) {
                switch ($case->default_case) {

                    case 'UPPER':
                        $attribute = strtoupper($attribute);
                        break;
                    case 'LOWER':
                        $attribute = strtolower($attribute);
                        break;
                    case 'PROPER':
                        $attribute = ucwords(strtolower($attribute));
                        break;

                    default:
                        # code...
                        break;
                }
                $text = str_replace("[:".$header.":]", $attribute, $text);
            }
        }
        return $text;
    }

    private function _apply_variable_rules($label)
    {
        $variable = $this->variables->where('label', $label)->first();
        if($variable) {
            $text = $variable->text;
            $returnData = ['rules_applied' => [], 'text' => $text];
            $attributes = json_decode($this->product->attributes, true);
            if($attributes && count($attributes)){
                foreach ($attributes as $header => $attribute) {
                    $text = $this->_replace_attribute($text, $header, $attribute);
                }

                foreach ($variable->rules as $rule) {
                    switch ($rule->condition) {
                        case 'HAS':
                            $i = ($rule->condition_case_sensetive) ? "i" : "";
                            $matchthis = preg_quote($rule->condition_value,'/');
                            $pattern = "/({$matchthis}?)/$i";

                            if(preg_match_all($pattern, $text, $matches, PREG_SET_ORDER)) {
                                foreach ($matches as $match) {
                                    $data['find'] = $match[0];
                                    $data['value'] = $rule->action_value;
                                    $full = ($rule->matched_only) ? false : true;
                                    $text = $this->_do_action($text, $rule->action, $data, $full);
                                }
                            }
                            break;
                        case  'DOES_NOT_HAVE':
                            if(strpos($page, 'index.php') === false) {
                                $data['find'] = $rule->condition_value;
                                $data['value'] = $rule->action_value;
                                $text = $this->_do_action($text, $rule->action, $data, true);
                            }
                            break;
                        case   'EQUALS':
                            if($rule->condition_value == "*") {
                                $text = trim($text);
                                if($text != ""){
                                    $data['find'] = $text;
                                    $data['value'] = $rule->action_value;
                                    $text = $this->_do_action($text, $rule->action, $data, true);
                                }
                            } else {
                                if ($rule->condition_value == $text) {
                                    $data['find'] = $rule->condition_value;
                                    $data['value'] = $rule->action_value;
                                    $text = $this->_do_action($text, $rule->action, $data, true);
                                }
                            }
                            break;
                        case   'RUN_ALWAYS':
                            $data['find'] = $rule->condition_value;
                            $data['value'] = $rule->action_value;
                            $full = ($rule->matched_only) ? false : true;
                            $text = $this->_do_action($text, $rule->action, $data, $full);
                            break;

                        default:
                            # code...
                            break;
                    }
                }
            }
            return $text;
        }
        return "";
    }

    private function _do_action($text, $action, $data, $full)
    {
        $replace = $data['value'];
        $find = $data['find'];
        $originalfind = $find;
        switch ($action) {

            case 'REPLACE_WITH':
                $text = $this->_replace($text, $find, $replace, $full);
                break;
            case 'APPEND':
                if ($full) {
                    $text = $text." ".$replace;
                } else {
                    $replace = $find." ".$replace;
                    $text = $this->_replace($text, $find, $replace, false);
                }
                break;
            case 'PREPEND':
                if ($full) {
                    $text = $replace." ".$text;
                } else {
                    $replace = $replace." ".$find;
                    $text = $this->_replace($text, $find, $replace, false);
                }
                break;
            case 'DELETE':
                if ($full) {
                    $text = "";
                } else {
                    $replace = "";
                    $text = $this->_replace($text, $find, $replace, false);
                }
                break;
            case 'CAPITALIZE':
                if ($full) {
                    $text = ucwords($text);
                } else {
                    $replace = ucwords($find);
                    $text = $this->_replace($text, $find, $replace, false);
                }
                break;
            case 'TOLOWER':
                if ($full) {
                    $text = strtolower($text);
                } else {
                    $find = $replace;
                    $replace = strtolower($replace);
                    $text = $this->_replace($text, $find, $replace, false);
                }
                break;
            case 'TOUPPER':
                if ($full) {
                    $text = strtoupper($text);
                } else {
                    $find = $replace;
                    $replace = strtoupper($find);
                    $text = $this->_replace($text, $originalfind, $replace, false);
                }
                break;
            case 'MAKE_PLURAL':
                $replace = str_plural($find);
                $text = $this->_replace($text, $find, $replace, false);
                break;
            case 'MAKE_SINGULAR':
                $replace = str_plural($find, 1);
                $text = $this->_replace($text, $find, $replace, false);
                break;
            case 'CLEAN_UP':
                $chars = str_split($replace);
                if($full) {
                    foreach ($chars as $cleanup) {
                        if($cleanup == 's' || $cleanup == 'S') {
                            $cleanup = " ";
                        }
                        $text = str_replace($cleanup, "", $text);
                    }
                } else {
                    $orginalFind = $find;
                    foreach ($chars as $cleanup) {
                        if($cleanup == 's' || $cleanup == 'S') {
                            $cleanup = " ";
                        }
                        $newReplace = str_replace($cleanup, "", $find);
                        dump($newReplace);
                    }
                    $text = $this->_replace($text, $orginalFind, $find, false);
                }
                break;

            default:
                # code...
                break;
        }
        return trim($text);
    }

    private function _replace($text, $matched, $replace, $full = false) {
        if($full) {
            return $replace;
        } else {
            $newText = str_replace($matched, $replace, $text);
            return $newText;
        }
    }

    private function _get_random($array)
    {
        if(count($array) > 1) {
            $randKey = array_rand($array,1);
            return $array[$randKey];
        } else {
            if(isset($array[0])) {
                return $array[0];
            } else {
                return null;
            }
        }
        //return random array element
    }

    private function _process_rule($rule, $text= "")
    {

    }


}