<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Backpack\CRUD\CrudTrait;

class SpinRule extends Model {

	use CrudTrait;

    /*
	|--------------------------------------------------------------------------
	| GLOBAL VARIABLES
	|--------------------------------------------------------------------------
	*/
	protected $dates = ['created_at','updated_at'];
	protected $table = 'spin_rules';
	protected $primaryKey = 'id';
	// protected $guarded = [];
	// protected $hidden = ['id'];
	protected $fillable = [
	    'order',
        'label',
        'description',
        'condition_case_sensetive',
        'condition',
        'condition_value',
        'action',
        'action_value',
        'matched_only',
        'sentence_id',
        'variable_id',
        'attribute_id',
	];
	public $timestamps = true;

	/*
	|--------------------------------------------------------------------------
	| FUNCTIONS
	|--------------------------------------------------------------------------
	*/

	/*
	|--------------------------------------------------------------------------
	| RELATIONS
	|--------------------------------------------------------------------------
	*/

	public function senetnce()
	{
		return $this->belongsTo('Sentence');
	}

	public function variable()
	{
		return $this->belongsTo('Variable');
	}

	public function attribute()
	{
		return $this->belongsTo('Attribute');
	}

	/*
	|--------------------------------------------------------------------------
	| SCOPES
	|--------------------------------------------------------------------------
	*/

	/*
	|--------------------------------------------------------------------------
	| ACCESORS
	|--------------------------------------------------------------------------
	*/

	/*
	|--------------------------------------------------------------------------
	| MUTATORS
	|--------------------------------------------------------------------------
	*/
}