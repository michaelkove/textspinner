<?php
namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\CreateSpinVariableCrudRequest as StoreRequest;
use App\Http\Requests\UpdateSpinVariableCrudRequest as UpdateRequest;
// use Backpack\CRUD\app\Http\Requests\CrudRequest as RegularRequest;
use Illuminate\Http\Request;
class SpinVariableCrudController extends CrudController {

    public function setup() {
        $this->crud->setModel("App\Models\SpinVariable");
        $this->crud->setRoute("admin/spin-variable");
        $this->crud->setEntityNameStrings('Variable', 'Variables');
        $this->crud->enableDetailsRow();
        $this->crud->allowAccess('details_row');
        $this->crud->setColumns([
                [
                   'name' => 'label',
                   'label' => "Label",
                    'type' => 'text',
                ],
                [
                   'name' => 'description',
                   'label' => "Description",
                    'type' => 'text',
                ],
                [
                   'name' => 'text',
                   'label' => "Text",
                    'type' => 'text',
                ],
                [
                   // n-n relationship (with pivot table)
                   'label' => "Global Rules", // Table column heading
                   'type' => "select_multiple",
                   'name' => 'rules', // the method that defines the relationship in your Model
                   'entity' => 'rules', // the method that defines the relationship in your Model
                   'attribute' => "label", // foreign key attribute that is shown to user
                   'model' => "App\Models\Rule", // foreign key model
                ],
            ]);
            // $this->crud->hasAccessOrFail('update');

    }
    // public function index()
    // {
    //     return \View::make('vendor.backpack.spin-variable.index',[]);

    // }

    public function create()
    {
        $attributes = \App\Models\Attribute::get();
        return view('crud::spin.create-variable', ['attributes' => $attributes, 'crud' => $this->crud]);
    }

    public function edit($id)
    {
        $attributes = \App\Models\Attribute::get();
        return view('crud::spin.edit-variable', ['attributes' => $attributes, 'crud' => $this->crud, 'variable' => $this->crud->getEntry($id)]);
    }

    public function store(StoreRequest $request)
    {
        return parent::storeCrud();
    }

     public function showDetailsRow($id)
    {
        $this->crud->hasAccessOrFail('details_row');
        $this->data['entry'] = $this->crud->getEntry($id);
        $this->data['crud'] = $this->crud;
        return view('crud::spin.partials.details-variable', $this->data);
    }

    public function update($id, UpdateRequest $request)
    {
        $spinVariable = \App\Models\SpinVariable::find($id);
        $spinVariable->text = $request->text;
        $spinVariable->description = $request->description;
        $spinVariable->save();
        \Alert::success("updated")->flash();

        return redirect('/admin/spin-variable');
        // return  parent::updateCrud();
    }

    public function order_rules(Request $request)
    {
        $orderData = $request->order;
        $updateOrders = [];

        foreach ($orderData as $order) {
            \App\Models\SpinRule::find($order['id'])->update(['order' => $order['order']]);
        }
        $success = true;
        $jsonResults = ['success' => $success, 'html' => ""];
        return \Response::json($jsonResults);
    }

    public function delete_rule(Request $request)
    {
        $id = $request->get('id');
        $rule = \App\Models\SpinRule::find($id);
        $rule->delete();
        $success = true;
        $jsonResults = ['success' => $success, 'html' => ""];
        return \Response::json($jsonResults);
    }

    public function add_rule(Request $request)
    {
        $ruleData = $request->get('rule');
        if(!$ruleData['action_value']) {
          $ruleData['action_value'] = "";
        }
        try {
            $rule = \App\Models\SpinRule::firstOrCreate($ruleData);
            $success = true;
        } catch (Exception $e) {
            $success = false;
        }
        $html = view('crud::spin.partials.variable-rule', ['rule' => $rule])->render();
        $jsonResults = ['success' => $success, 'html' => $html];
        return \Response::json($jsonResults);
    }

}