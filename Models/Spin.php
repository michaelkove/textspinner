<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class Spin extends Model {

	use CrudTrait;

    /*
	|--------------------------------------------------------------------------
	| GLOBAL VARIABLES
	|--------------------------------------------------------------------------
	*/
	protected $table = 'spins';
	protected $primaryKey = 'id';
	protected $fillable = [
		'status',
		'name',
		'description',
		'spincode',
	];
	public $timestamps = true;

	/*
	|--------------------------------------------------------------------------
	| FUNCTIONS
	|--------------------------------------------------------------------------
	*/

	/*
	|--------------------------------------------------------------------------
	| RELATIONS
	|--------------------------------------------------------------------------
	*/
	public function products()
	{
		return $this->hasMany('\App\Models\Product', 'product_id', 'id');
	}
	/*
	|--------------------------------------------------------------------------
	| SCOPES
	|--------------------------------------------------------------------------
	*/

	public function spin_groups()
	{
		return $this->hasMany('\App\Models\SpinGroup');
	}
	/*
	|--------------------------------------------------------------------------
	| ACCESORS
	|--------------------------------------------------------------------------
	*/
	public function add_sentences()
	{
        return '<a href="/admin/spin/'.$this->id.'/add-sentences" data-toggle="tooltip" title="Add Sentences to this spin" class="btn btn-xs btn-info"><i class="fa fa-refresh" ></i> Add Sentences</a>';
	}
	/*
	|--------------------------------------------------------------------------
	| MUTATORS
	|--------------------------------------------------------------------------
	*/
}