<?php
namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\CreateSpinCrudRequest as StoreRequest;
use App\Http\Requests\UpdateSpinCrudRequest as UpdateRequest;
// use Backpack\CRUD\app\Http\Requests\CrudRequest as RegularRequest;
use Illuminate\Http\Request;
use App\Spinner\Spinner;

class SpinCrudController extends CrudController {

    public function setup() {
        $this->crud->setModel("App\Models\Spin");
        $this->crud->setRoute("admin/spin");
        $this->crud->setEntityNameStrings('Spin', 'Spins');
        $this->crud->addButtonFromModelFunction('line', 'add_sentences', 'add_sentences', 'beginning'); // add a button whose HTML is returned by a method in the CRUD model
        // $this->crud->enableDetailsRow();
        // $this->crud->allowAccess('details_row');
        $this->crud->setColumns([
                [
                   'name' => 'name',
                   'label' => "Name",
                    'type' => 'text',
                ],
                [
                   'name' => 'description',
                   'label' => "Description",
                    'type' => 'text',
                ],
            ]);
        $this->crud->addField(['name' => 'name',  'type'    => "text", 'label' => 'Name',  'wrapperAttributes' => ['class' => 'col-md-4']], 'both');
        $this->crud->addField(['name' => 'description',  'type'    => "text", 'label' => 'Description',  'wrapperAttributes' => ['class' => 'col-md-4']], 'both');
        $this->crud->addField(
            [ // select_from_array
                    'name' => 'status',
                    'label' => "Status",
                    'type' => 'select_from_array',
                    'options' => ['1' => "Active", '0' => "Disabled"],
                    'allows_null' => false,
                    'default' => '1',
                    'wrapperAttributes' => ['class' => 'col-md-4']
                ], 'both');
    }


    public function add_sentence($id, Request $request)
    {
        $spinGroupId = $request->get('spingroupid');
        $spin = \App\Models\Spin::find($id);
        $order = $request->get('order');
        $sentenceData = [];
        $spinGroup = \App\Models\SpinGroup::find($spinGroupId);
        $sentence = \App\Models\Sentence::create(['spincode' => $request->get('text')]);
        $sentenceData[$sentence->id] = ['order' => $order];
        $spinGroup->sentences()->syncWithoutDetaching($sentenceData);
        $html = view('crud::spin.partials.spin-group', ['spinGroup' => $spinGroup])->render();
        $success = true;
        $jsonResults = ['success' => $success, 'html' => $html];
        return \Response::json($jsonResults);
    }

     public function get_sentence($id, Request $request)
    {
        $sentence = \App\Models\Sentence::find($request->get('sentenceId'));
        $success = true;
        $jsonResults = ['success' => $success, 'sentence' => $sentence->spincode];
        return \Response::json($jsonResults);
    }

    // public function add_sentence($id, Request $request)
    // {
        //post here with sentence data
    // }

    public function update_spin_groups_order($id, Request $request)
    {
        $orders = $request->get('orders');
        $fixOrders = [];
        $ids = [];
        foreach ($orders as $order) {
            \App\Models\SpinGroup::where('id', $order['id'])->update(['order' => $order['order']]);
        }
        $success = true;
        $jsonResults = ['success' => $success, 'html' => ""];
        return \Response::json($jsonResults);
    }

    public function update_sentence($id, Request $request)
    {
        $sentenceId = $request->get('sentenceId');
        $spinGroupId = $request->get('spinGroupId');
        $sentence = \App\Models\Sentence::find($sentenceId);
        $sentence->spincode = $request->get('text');
        $sentence->save();

        $spinGroup = \App\Models\SpinGroup::find($spinGroupId);
        $html = view('crud::spin.partials.spin-group', ['spinGroup' => $spinGroup])->render();
        $success = true;
        $jsonResults = ['success' => $success, 'html' => $html];
        return \Response::json($jsonResults);
    }

    public function remove_sentence($id, Request $request)
    {
        $sentenceId = $request->get('sentenceId');
        $spinGroupId = $request->get('spinGroupId');
        $sentence = \App\Models\Sentence::find($sentenceId);
        $sentence->delete();
        $spinGroup = \App\Models\SpinGroup::find($spinGroupId);
        $html = view('crud::spin.partials.spin-group', ['spinGroup' => $spinGroup])->render();
        $success = true;
        $jsonResults = ['success' => $success, 'html' => $html];
        return \Response::json($jsonResults);
    }

    public function remove_spin_group($id, Request $request)
    {
        $spinGroupId = $request->get('spinGroupId');
        $spinGroup = \App\Models\SpinGroup::find($spinGroupId);
        foreach ($spinGroup->sentences as $sentence) {
            $sentence->delete();
        }
        $spinGroup->delete();
        $success = true;
        $jsonResults = ['success' => $success, 'html' => ""];
        return \Response::json($jsonResults);
    }

    public function add_spin_group_tag($id, Request $request)
    {
        $spin = \App\Models\Spin::find($id);
        $attributes = ($request->get('attributes')) ?? "";
        $tag = $request->get('tag');
        $type = $request->get('type');

        if($type == 'SINGLE' || $type == 'OPEN_TAG') {
            $label = "<{$tag} {$attributes}>";
        } elseif ($type == 'CLOSE_TAG') {
            $label = "</{$tag}>";
        }
        $label = htmlspecialchars($label);
        $spinGroup = \App\Models\SpinGroup::create(['wrap_tags_json' => $attributes, 'spin_id' => $spin->id, 'label' => $label, 'order'=> $request->get('order'), 'type' => $type]);
        $html = view('crud::spin.partials.spin-group-tag', ['spinGroup' => $spinGroup])->render();
        $success = true;
        $jsonResults = ['success' => $success, 'html' => $html];
        return \Response::json($jsonResults);
    }
    public function add_spin_group($id, Request $request)
    {
        $spin = \App\Models\Spin::find($id);
        $rawTags = $request->get('tags');
        $jsonTags = json_encode($rawTags);
        $spinGroup = \App\Models\SpinGroup::create(['wrap_tags_json' => $jsonTags, 'spin_id' => $spin->id, 'label' => $request->get('label'), 'order'=> $request->get('order'), 'type' => 'SENTENCE']);
        $html = view('crud::spin.partials.spin-group', ['spinGroup' => $spinGroup])->render();
        $success = true;
        $jsonResults = ['success' => $success, 'html' => $html];
        return \Response::json($jsonResults);
        //post here with bl1ock order
        //create empty spin_group
    }

    public function update_spin_group_tags($id, Request $request)
    {
        $spinGroupId = $request->get('spin_group_id');
        $label = $request->get('label');
        $spinGroup = \App\Models\SpinGroup::find($spinGroupId);
        $rawTags = $request->get('tags');
        $jsonTags = json_encode($rawTags);
        $spinGroup->wrap_tags_json = $jsonTags;
        $spinGroup->label = $label;
        $spinGroup->save();

        $html = view('crud::spin.partials.spin-group', ['spinGroup' => $spinGroup])->render();
        $success = true;
        $jsonResults = ['success' => $success, 'html' => $html];
        return \Response::json($jsonResults);
    }

    public function add_sentences($id)
    {
        $products = \App\Models\Product::get();
        $spin = \App\Models\Spin::where('id', $id)->with(['spin_groups' => function($query) {
            $query->orderBy('order', 'ASC');
        }])->with('spin_groups.sentences')->first();
        $data['crud'] = $this->crud;
        $data['spin']  = $spin;
        $data['products']  = $products;
        $data['attributes'] = \App\Models\Attribute::get();
        $data['spinVariables'] =  \App\Models\SpinVariable::get();
        return view('crud::spin.create', $data);
    }

    public function live_preview($id, Request $request)
    {
        $productId = $request->get('product_id');

        $spin = \App\Models\Spin::where('id', $id)->with('spin_groups')->with('spin_groups.sentences')->first();
        $product = \App\Models\Product::where('id', $productId)->first();
        $variables = \App\Models\SpinVariable::get();

        $spinner = new Spinner(['product' => $product, 'spin'=> $spin, 'variables' => $variables]);
        $text = $spinner->process();

        $html = $text;
        $success = true;
        $jsonResults = ['success' => $success, 'html' => $html];
        return \Response::json($jsonResults);
    }

    public function create()
    {

		return parent::create();
    }

    public function store(StoreRequest $request)
    {
        return parent::storeCrud();
    }

    public function update(UpdateRequest $request)
    {
        return parent::updateCrud();
	}

}